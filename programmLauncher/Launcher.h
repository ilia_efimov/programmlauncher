#include <iostream>
#include <string>
#include <memory>

#pragma once

#define PROPERTIES_FILE_NAME "config.properties"
#define JAVA_HOME_ENVVARIABLE_NAME  "JAVA_HOME"
#define JAVA_VERSION_PARAMETER "-version"

#define PROPERTY_ALTERNATIVE_JVM_PATH  "alternativeJVMPath"
#define PROPERTY_LAUNCH_PROGRAMM  "launchProgramm"
#define PROPERTY_JVM_LAUNCH_PARAMETERS  "jvmLaunchParameters"
#define PROPERTY_COMMAND_LINE_PARAMS  "comandLineParams"

typedef struct PropertiesParameters {
	std::string alternativeJVMPath;
	std::string launchProgramm;
	std::string jvmLaunchParameters;
	std::string comandLineParams;
};

class Launcher {
public:
	std::unique_ptr<PropertiesParameters> LoadProperties();
	virtual void LaunchProgramm() = 0;
};

