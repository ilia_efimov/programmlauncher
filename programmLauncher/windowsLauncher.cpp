#include "Launcher.h"
#include <iostream>
#include <string>
#include "main.h"
#include "windowsLauncher.h"
#include <cstring>

using namespace std;

#ifdef WINDOWS

#include <windows.h>

#define PART_OF_JAVA_PATH "\\bin\\java"


void WindowsLauncher::LaunchProgramm() {
	unique_ptr<PropertiesParameters> properties = LoadProperties();
	if (properties == nullptr) {
		cout << "Error reading properties file" << endl;
		getchar();
		return;
	}

	const char *javaHomePath = nullptr; 
	if (properties->alternativeJVMPath.length() > 0) {
		javaHomePath = properties->alternativeJVMPath.c_str();
	} else {
		javaHomePath = getenv(JAVA_HOME_ENVVARIABLE_NAME);
	}

	if (javaHomePath == NULL || strlen(javaHomePath) == 0) {
		cout << "Please set environment variable JAVA_HOME with actual java version or set alternativeJVMPath parameter into launchconfig.properties file" << endl;
		getchar();
		return;
	}
	
	if (properties->launchProgramm.length() == 0)  {
		cout << "Please set launchProgramm parameter into launchconfig.properties file" << endl;
		getchar();
		return;
	}
	
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	ZeroMemory(&pi, sizeof(pi));

	string javaLaunchString = string(javaHomePath) + PART_OF_JAVA_PATH + " -jar " + properties->jvmLaunchParameters + " " + properties->launchProgramm + " " + properties->comandLineParams;
	si.dwFlags = STARTF_USESHOWWINDOW;
	if (!CreateProcessA(
		NULL, const_cast<char *>(javaLaunchString.c_str()), 
		NULL,  NULL, FALSE,  CREATE_NO_WINDOW,	NULL,	NULL,	&si,	&pi	)) {
		cout << "Process cannot be run!"  << endl;
		getchar();
	}
	return;
}

#endif