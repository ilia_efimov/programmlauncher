#include "Launcher.h"
#include <map>
#include <fstream>
#include <memory>
#include <cstring>
#include <string>
#include <iostream>

using namespace std;

unique_ptr<PropertiesParameters> Launcher::LoadProperties() {
	ifstream file("launchconfig.properties");
	if (!file) {
		return nullptr;
	}
	string line;
	map<string, string> parameters;
	map<string, string>::iterator paramsIterator;

	while (!file.eof()) {
		getline(file, line, '\n');
		int idx = line.find('=');
		if (idx > 0) {
			string key = line.substr(0, idx);	
			if (idx + 1 < line.length() - 1) {
				string value = line.substr(idx + 1, line.length() - 1);	
				parameters[key] = value;
			}
		}
	}

	unique_ptr<PropertiesParameters> propertiesParameters(new PropertiesParameters());	

	for (paramsIterator = parameters.begin(); paramsIterator != parameters.end() ;) {
		if (paramsIterator->first == PROPERTY_ALTERNATIVE_JVM_PATH) {
			propertiesParameters->alternativeJVMPath = paramsIterator->second;
		}
		if (paramsIterator->first == PROPERTY_LAUNCH_PROGRAMM) {
			propertiesParameters->launchProgramm = paramsIterator->second;
		}
		if (paramsIterator->first == PROPERTY_JVM_LAUNCH_PARAMETERS) {
			propertiesParameters->jvmLaunchParameters = paramsIterator->second;
		}
		if (paramsIterator->first == PROPERTY_COMMAND_LINE_PARAMS) {
			propertiesParameters->comandLineParams = paramsIterator->second;
		}

		paramsIterator ++;
	}

	return propertiesParameters;
}
