#include "Launcher.h"
#include <iostream>
#include <string>
#include <memory>
#include "main.h"

#pragma once

#ifdef WINDOWS

#include <windows.h>

class WindowsLauncher : public Launcher {
public:
	void LaunchProgramm();
private:

};


#endif
