#include "main.h"
#include "Launcher.h"
#include <memory>


#ifdef WINDOWS
#include "windowsLauncher.h"
#endif


using namespace std;

int main(void ) {
	unique_ptr<Launcher> launcher;

#ifdef WINDOWS
	launcher = unique_ptr<Launcher>(new WindowsLauncher());
#endif
	
	if (launcher != nullptr) {
		launcher->LoadProperties();
		launcher->LaunchProgramm();
	}

	return 0;
}